package mongodb2.demo;

import com.mongodb.*;

public class mongoInsert {
    public static void insert() throws Exception{
        //获取连接mongodb数据库
        MongoClient client = new MongoClient("localhost",27001);
        //连接mldn的数据库
        DB db = client.getDB("mldn");
        //进行授权登录
        if(db.authenticate("root","root".toCharArray())){
            //连接deptcol的集合
            DBCollection dbCollection = db.getCollection("deptcol");
            for (int i = 0; i < 1000; i++) {
                //构造一个json的数据
                BasicDBObject basicDBObject = new BasicDBObject();
                basicDBObject.append("deptno" , i);
                basicDBObject.append("deptname","test" + i);
                basicDBObject.append("sex",i%2==0?"男":"女");
                //插入集合
                WriteResult result = dbCollection.insert(basicDBObject);
                System.out.println(result.getN());
            }
        }
        client.close();
    }
}
