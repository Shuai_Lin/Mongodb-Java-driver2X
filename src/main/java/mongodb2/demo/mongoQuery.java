package mongodb2.demo;

import com.mongodb.*;

public class mongoQuery {
    public static void query() throws Exception{
        //获取连接mongodb数据库
        MongoClient client = new MongoClient("localhost",27001);
        //连接mldn的数据库
        DB db = client.getDB("mldn");
        //进行授权登录
        if(db.authenticate("root","root".toCharArray())){
            //连接deptcol的集合
            DBCollection dbCollection = db.getCollection("deptcol");
            DBCursor cursor = dbCollection.find();
            while (cursor.hasNext()){
                System.out.println(cursor.next());
            }
        }
        client.close();
    }
}
