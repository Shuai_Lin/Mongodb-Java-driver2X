package mongodb2.demo;

import com.mongodb.DB;
import com.mongodb.MongoClient;

public class mongoConnection {
    public static void main(String[] args) throws Exception{
        //获取连接mongodb数据库
        MongoClient client = new MongoClient("localhost",27001);
        //连接mldn的数据库
        DB db = client.getDB("mldn");
        //进行授权登录
        if(db.authenticate("root","root".toCharArray())){
            for (String name : db.getCollectionNames()) {
                System.out.println("集合名称：" + name);
            }
            db.getCollection("deptno").drop();
        }
        client.close();

        //插入数据
        mongoInsert.insert();

        //查询数据
        mongoQuery.query();

        //分页查询
        mongoQueryLimit.queryLimit();

        //范围查询
        mongoQueryByIn.queryByIn();

        //正则查询
        mongoQueryByRegex.queryByRegex();

        //条件查询
        mongoQueryByCondition.queryByCondition();

        //更新数据
        mongoUpdate.update();

        //删除数据
        mongoDelete.delete();

    }
}
