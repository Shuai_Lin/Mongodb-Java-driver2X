package mongodb2.demo;

import com.mongodb.*;

public class mongoQueryByCondition {
    public static void queryByCondition() throws Exception{
        //获取连接mongodb数据库
        MongoClient client = new MongoClient("localhost",27001);
        //连接mldn的数据库集合
        DB db = client.getDB("mldn");
        //进行授权登录
        if(db.authenticate("root","root".toCharArray())){
            //连接deptcol的集合
            DBCollection dbCollection = db.getCollection("deptcol");
            DBObject dbObject = new BasicDBObject(); //设置过滤的条件
            dbObject.put("deptno",new BasicDBObject("$gte",0).append("$lte",50));
            //跳过10条，显示10条
            DBCursor cursor = dbCollection.find(dbObject).skip(10).limit(100);
            while (cursor.hasNext()){
                System.out.println(cursor.next());
            }
        }
        client.close();
    }
}
