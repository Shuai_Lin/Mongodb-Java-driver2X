package mongodb2.demo;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

public class mongoQueryLimit {
    public static void queryLimit() throws Exception{
        //获取连接mongodb数据库
        MongoClient client = new MongoClient("localhost",27001);
        //连接mldn的数据库集合
        DB db = client.getDB("mldn");
        //进行授权登录
        if(db.authenticate("root","root".toCharArray())){
            //连接deptcol的集合
            DBCollection dbCollection = db.getCollection("deptcol");
            //跳过10条，显示10条
            DBCursor cursor = dbCollection.find().skip(10).limit(10);
            while (cursor.hasNext()){
                System.out.println(cursor.next());
            }
        }
        client.close();
    }
}
