package mongodb2.demo;

import com.mongodb.*;

public class mongoQueryByIn {
    public static void queryByIn() throws Exception{
        //获取连接mongodb数据库
        MongoClient client = new MongoClient("localhost",27001);
        //连接mldn的数据库集合
        DB db = client.getDB("mldn");
        //进行授权登录
        if(db.authenticate("root","root".toCharArray())){
            //连接deptcol的集合
            DBCollection dbCollection = db.getCollection("deptcol");
            DBObject dbObject = new BasicDBObject(); //设置过滤的条件
            dbObject.put("deptno",new BasicDBObject("$in",new int[]{1,2,3,4,5,6,7,8,9}));
            //跳过10条，显示10条
            DBCursor cursor = dbCollection.find(dbObject).skip(1).limit(100);
            while (cursor.hasNext()){
                System.out.println(cursor.next());
            }
        }
        client.close();
    }
}
