package mongodb2.demo;

import com.mongodb.*;

public class mongoDelete {
    public static void delete() throws Exception{
        //获取连接mongodb数据库
        MongoClient client = new MongoClient("localhost",27001);
        //连接mldn的数据库集合
        DB db = client.getDB("mldn");
        //进行授权登录
        if(db.authenticate("root","root".toCharArray())){
            //连接deptcol的集合
            DBCollection dbCollection = db.getCollection("deptcol");
            DBObject dbObject = new BasicDBObject();
            dbObject.put("deptno",new BasicDBObject("$gte",0).append("$lte",5));
            WriteResult result = dbCollection.remove(dbObject);
            System.out.println(result.getN());
        }
        client.close();
    }
}
